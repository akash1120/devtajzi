<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['al_add_item_success'] = 'A new item has been added to the menu.';
$lang['al_add_item_error'] = 'An error occurred and item has not been added to the menu.';
$lang['al_update_item_success'] = 'Changes for the selected menu item have been saved.';
$lang['al_update_item_error'] = 'An error occurred and the changes for the selected menu item haven\'t been saved.';
$lang['al_delete_item_success'] = 'The selected item has been removed from the menu.';
$lang['al_delete_item_error'] = 'An error occurred and the selected item could not be removed from the menu or does not exist.';
$lang['al_has_children_error'] = 'You can not remove items from the menu, which includes more items.';

$lang['al_add_group_success'] = 'New navigation group has been added.';
$lang['al_add_group_error'] = 'An error occurred and the navigation group has not been added.';
$lang['al_update_group_success'] = 'Changes for selected navigation group have been saved.';
$lang['al_update_group_error'] = 'An error occurred and the changes for the selected navigation group haven\'t been saved.';
$lang['al_delete_group_success'] = 'A select navigation group has been deleted.';
$lang['al_delete_group_error'] = 'An error occurred and the selected navigation group could not be removed or does not exist.';
$lang['megamenu_are_you_sure'] = 'Are you sure ?';
$lang['megamenu_delete_sub_desc'] = 'Do you want to delete this item.';
$lang['megamenu_manage_navigation'] = 'Manage navigation';
$lang['megamenu_add_group'] = 'Add navigation group';
$lang['megamenu_no_group'] = 'There is no navigation groups.';
$lang['megamenu_add_item'] = 'Add item';
$lang['megamenu_menu_name'] = 'Menu Name';
$lang['megamenu_menu_url'] = 'URL';
$lang['megamenu_menu_parent'] = 'Parent';
$lang['megamenu_menu_bg_color'] = 'Menu item BG color';
$lang['megamenu_bg_color'] = 'Menu BG color';
$lang['megamenu_font_color'] = 'Menu font color';
$lang['megamenu_font'] = 'Menu font';
$lang['megamenu_font_size'] = 'Menu font size';
$lang['megamenu_font_weight'] = 'Menu font weight';
$lang['megamenu_bg_image'] = 'Menu BG image';
$lang['megamenu_bg_size'] = 'Menu BG size';
$lang['megamenu_bg_repeat'] = 'Menu BG repeat';
$lang['megamenu_bg_position'] = 'Menu BG position';
$lang['megamenu_menu_bg_hover_color'] = 'Menu item BG hover color';
$lang['megamenu_menu_bg_image'] = 'Menu item BG image';
$lang['megamenu_menu_bg_repeat'] = 'Menu item BG repeat';
$lang['megamenu_menu_bg_size'] = 'Menu item BG size';
$lang['megamenu_menu_icon'] = 'Fontawesome icon';
$lang['megamenu_grid_length'] = 'Grid Length';
$lang['megamenu_menu_width'] = 'Menu Width';
$lang['megamenu_save_changes'] = 'Save changes';
$lang['megamenu_item_added'] = 'Item added successfully';
$lang['megamenu_item_saved'] = 'Item saved successfully';
$lang['megamenu_item_deleted'] = 'Item deleted successfully';
$lang['megamenu_navigation'] = 'Navigation:';
$lang['megamenu_show_only_group'] = 'Show only this group(s): <br><small>Select none for public.</small>';







/* End of file adjacency_list_lang.php */
/* Location: ./application/languages/english/adjacency_list_lang.php */
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
    $getAllGeoZones = $this->site->getAllGeoZones();
    $getAllOrderStatus = $this->site->getAllOrderStatus();
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_payment_method'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open_multipart("products/edit_payment_methods/".$payment_method->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('update_info'); ?></p>

            <ul class="nav nav-tabs">
                <li id="offline" class="<?= $payment_method->secret_code == 'offline' ? 'active' : 'hide' ?> tab_payment_method"><a data-toggle="tab" href="#home">OFFLINE</a></li>
                <li id="cod" class="<?= $payment_method->secret_code == 'cod' ? 'active' : 'hide' ?> tab_payment_method"><a data-toggle="tab" href="#home1">Cash On Delivery</a></li>
                <li id="paypal" class="<?= $payment_method->secret_code == 'paypal' ? 'active' : 'hide' ?> tab_payment_method"><a data-toggle="tab" href="#menu1">PayPal</a></li>
                <li id="paytabs" class="<?= $payment_method->secret_code == 'paytabs' ? 'active' : 'hide' ?> tab_payment_method"><a data-toggle="tab" href="#menu2">PayTabs</a></li>
            </ul>

            <div class="tab-content">
                <div id="home" class="tab-pane fade <?= $payment_method->secret_code == 'offline' ? 'in active' : '' ?>">
                    <h3>OFFLINE/Bank Transfer</h3>
                    <p>Its Offline/Bank Transfer process. Type the below information to configure this as payment method.</p>
                </div>
                <div id="home1" class="tab-pane fade <?= $payment_method->secret_code == 'cod' ? 'in active' : '' ?>">
                    <h3>Cash On Delivery</h3>
                    <p>Cash on delivery (COD), sometimes called collect on delivery, is the sale of goods by mail order where payment is made on delivery rather than in advance.</p>
                </div>
                <div id="menu1" class="tab-pane fade <?= $payment_method->secret_code == 'paypal' ? 'in active' : '' ?>">
                    <h3>PayPal</h3>
                    <p>PayPal Holdings, Inc. is an American company operating a worldwide online payments system that supports online money transfers and serves as an electronic alternative to traditional paper methods like checks and money orders.</p>
                    <div class="form-group">
                        <label for="paypal_email">Email</label>
                        <?= form_input('paypal_email', set_value('paypal_email', ($payment_method ? $payment_method->paypal_email : '')), 'class="form-control" id="paypal_email" '); ?>
                    </div>
                    <div class="form-group">
                        <label for="paypal_password">Password</label>
                        <?= form_input('paypal_password', set_value('paypal_email', ($payment_method ? $payment_method->paypal_password : '')) , 'class="form-control" id="paypal_password" '); ?>
                    </div>
                    <hr/>

                </div>
                <div id="menu2" class="tab-pane fade <?= $payment_method->secret_code == 'paytabs' ? 'in active' : '' ?>">
                    <h3>PayTabs</h3>
                    <p>PayTabs is a financial technology firm that provides payment processing and fraud prevention solutions for merchants and individuals across the MENA region.</p>
                    <div class="form-group">
                        <label for="merchant_id">Merchant ID</label>
                        <?= form_input('merchant_id', set_value('paypal_email', ($payment_method ? $payment_method->merchant_id : '')), 'class="form-control" id="merchant_id" '); ?>
                    </div>
                    <div class="form-group">
                        <label for="secret_key">Secret Key</label>
                        <?= form_input('secret_key',  set_value('paypal_email', ($payment_method ? $payment_method->secret_key : '')), 'class="form-control" id="secret_key" '); ?>
                    </div>
                    <div class="form-group">
                        <label for="url_redirect">URL Redirect</label>
                        <?= form_input('url_redirect',  set_value('paypal_email', ($payment_method ? $payment_method->url_redirect : '')), 'class="form-control" id="url_redirect" '); ?>
                    </div>
                    <hr/>
                </div>
            </div>

            <div class="form-group">
                <?= lang('payment_name', 'name'); ?>
                <?= form_input('name', set_value('name',$payment_method->name), 'class="form-control gen_slug" id="name" required="required"'); ?>
            </div>

            <div class="form-group">
                <?= lang('payment_method_code', 'code'); ?>
                <?= form_input('code', set_value('code',$payment_method->code), 'class="form-control" id="code" required="required"'); ?>
            </div>

            <div class="form-group">
                <label for="total">Total<span style="font-size: 10px;">(The checkout total the order must reach before this payment method becomes active.)</span></label>
                <?= form_input('total', set_value('total',$payment_method->total), 'class="form-control" id="total"'); ?>
            </div>

            <div class="form-group">
                <label for="order-status">Order Status</label>
                <select id="order_status" name="order_status" class="form-control">
                    <?php foreach ($getAllOrderStatus as $order_status){?>
                        <option <?= $payment_method->order_status == $order_status ? 'selected' : '' ; ?> value="<?= $order_status ?>"><?= $order_status?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-group">
                <label for="geo_zone">Geo Zones</label>
                <select id="geo_zone" name="geo_zone" class="form-control">
                    <option value="">All Zones</option>
                    <?php foreach ($getAllGeoZones as $zone){?>
                        <option <?= $payment_method->geo_zone_id == $zone->geo_zone_id ? 'selected' : '' ; ?> value="<?= $zone->geo_zone_id ?>"><?= $zone->name?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-group">
                <label for="total">Sort Order</label>
                <?= form_input('sort_order', set_value('sort_order',$payment_method->sort_order), 'class="form-control" id="sort_order"'); ?>
            </div>


            <div class="form-group">
                <?= lang("payment_image", "image") ?>
                <input id="image" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false"
                       class="form-control file">
            </div>


            <div class="form-group all">
                <?= lang('status', 'status'); ?><br/>
                <input id="status" class="form-control tip" type="checkbox" name="status" value="1" <?= $payment_method->status == 1 ? 'checked' : '' ?>> Check that if you want to enable it
            </div>

            <div class="form-group all">
                <?= lang('payment_method_instruction', 'instruction'); ?><br/>
                <textarea name="instruction" id="instruction" class="form-control tip"><?= $payment_method->instruction; ?></textarea>
            </div>

            <input name="secret_code" type="hidden" id="secret_code" value="<?= $payment_method->secret_code; ?>">

        </div>
        <div class="modal-footer">
            <?php echo form_submit('edit_payment_method', lang('edit_payment_method'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
<script>
    $(document).ready(function() {
        $('.gen_slug').change(function(e) {
            getSlug($(this).val(), 'category');
        });
    });
</script>

<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
    $getAllGroups = $this->site->getAllVariantsGroup();
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_variant'); ?></h4>
        </div>
        <?php echo admin_form_open("products/edit_variant_group/" . $variant->id); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <label for="varient_group_name">Group</label>
                <select class="form-control" name="variant_group_id" id="variant_group_id">
                    <?php foreach ($getAllGroups as $rec){?>
                        <option <?=($variant->variant_id == $rec->id) ? 'selected' : ''  ?> value="<?= $rec->id; ?>"><?= $rec->name; ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-group">
                <label class="control-label" for="name"><?php echo $this->lang->line("name"); ?></label>
                <div class="controls"> <?php echo form_input('name', $variant->name, 'class="form-control" id="name" required="required"'); ?> </div>
            </div>

            <div class="form-group">
                <label class="control-label" for="name"><?php echo $this->lang->line("code"); ?></label>
                <div class="controls"> <?php echo form_input('code', $variant->code, 'class="form-control" id="code" required="required"'); ?> </div>
            </div>

            <div class="form-group">
                <?= lang("varient_image", "image") ?>
                <input id="image" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false"
                       class="form-control file">
            </div>

        </div>
        <div class="modal-footer">
            <?php echo form_submit('edit_variant', lang('edit_variant'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
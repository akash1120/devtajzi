<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style>
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    .box .box-header {
        height: 50px;
    }
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

<style></style>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-home"></i>Seo Settings</h2>

    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext">Seo Settings for all pages</p>

                <div class="table-responsive">
<div class="container col-sm-8" style="padding: 50px;">

    <table id="example" class="display" style="width:100%">
        <thead>
        <tr>
            <th>Serial #</th>
            <th>Page Slug</th>
            <th>Meta title</th>
            <th>Meta Description</th>
            <th>Update</th>


        </tr>
        </thead>
        <tbody>

        <?php foreach ($slug_array_data as $key => $data){ ?>
            <tr>
                <td><?= $data['id'] ?></td>
                <td><?= $data['slug'] ?></td>
                <td><?= $data['meta_title'] ?></td>
                <td><?= $data['meta_description'] ?></td>
                <td> <a class="btn btn-success seoeditimage" data_slug="<?= $data['slug'];?>" data_meta_title="<?= $data['meta_title'];?>" data_meta_description="<?= $data['meta_description'];?>"  ><i class="fa fa-edit"></i></a> </td>
 


            </tr>
        <?php } ?>

        </tbody>
        <tfoot>
        <tr>
            <th>Serial #</th>
            <th>Page Slug</th>
            <th>Meta title</th>
            <th>Meta Description</th>
            <th></th>
        </tr>
        </tfoot>
    </table>
</div>
               </div>

            </div>

        </div>
    </div>
</div>





<script>
    $(document).ready(function() {
        // Setup - add a text input to each footer cell
        $('#example tfoot th').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" class="btm_search" placeholder="Search '+title+'" />' );
        } );

        // DataTable
        var table = $('#example').DataTable();

        // Apply the search
        table.columns().every( function () {
            var that = this;

            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );
        } );
        $(".btm_search:last").hide();
        //
       // $('#meta_description').redactor('core.destroy');


    } );
    $('.seoeditimage').click(function() {
        var slug = $(this).attr('data_slug');
        var meta_title = $(this).attr('data_meta_title');
        var meta_description = $(this).attr('data_meta_description');


        $('#slug').val(slug);
        $('#meta_title').val(meta_title);
        $('#meta_description').redactor();
        $('#meta_description').val(meta_description);
       // $('#meta_description').val(meta_description);
        //$('#meta_description').trigger('change');
       // $('#meta_description').editor.setValue('test');
        $('#meta_description').redactor('destroy');
       // $('#meta_description').redactor('stop');
        $('.editseo').modal('show');


    });
</script>



<div class="modal fade editseo" id="myModalee" >
    <div class="modal-dialog modal-md" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <h4 class="modal-title" id="myModalLabel">Meta Settings</h4>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                    <p class="introtext"><?php echo lang('update_info'); ?></p>
                    <?php
                    $attrib = array('role' => 'form');
                    echo admin_form_open_multipart("marketing/update_meta_settings", $attrib)
                    ?>
                    <div class="col-md-12">



                        <div class="panel panel-default padding10 border_radius3" style="padding: 10px 10px !important;">
                            <div class="row">

                                    <input name="slug" type="hidden" id="slug" value="">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang('title', 'link1'); ?>
                                        <?= form_input('meta_title',set_value('meta_title'), 'class="form-control tip" id="meta_title"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang('description', 'caption1'); ?>
                                        <textarea name="meta_description" class="form-control border_radius3 tip" id="meta_description"></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <?php echo form_submit('edit_meta_setting', 'Submit', 'class="btn btn-primary btn_edit_product_custom"'); ?>
                        </div>

                    </div>
                    <?= form_close(); ?>


                </div>
                <div class="clearfix"></div>
            </div>
            <!--<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

        <!-- /.modal-content -->
        </div>
        <div class="clearfix"></div>
        <!-- /.modal-dialog -->
    </div>
</div>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $is_cod_active = $this->site->getActivePaymentMethod('COD', 'cod');
if ($is_cod_active) {
    $cod_data = $this->site->getActivePaymentMethodData('COD', 'cod');
}
?>
<style>
    .small_payment_instruction p,table{
        margin-left: 10px;
    }
    .no-border-radius {
        border-radius: 0px;
    }

    .padding0 {
        padding: 0px;
    }

    .padding2 {
        padding: 3px;
    }

    .width {
        width: 100%;
    }

    input[name=address] + span::before {
        float: right;
        margin: 5px;
        margin-right: 10px;
    }

    input[class=payment_method] + span::before {
        padding-left: 1px;
        top: 7px;
        right: 10px;
        width: 20px;
        line-height: 18px;
        /* float: right;
         margin: 5px;
         margin-right: 10px;*/
    }

    .pt5 {
        padding-top: 5px;
    }

    .address_primary:after {
        content: '';
        position: relative;
        top: 0;
        right: -7px;
        width: 0;
        height: 0;
        border-color: #50AE55 transparent #50AE55 #50AE55;
        border-style: solid;
        border-width: 10px 7px 10px 0;
    }

    .ptb10 {
        padding-top: 10px;
        padding-bottom: 10px;
    }

    .method > li {
        border-left: 0px;
        border-right: 0px;
    }

    .paymentmthds > li {
        padding-top: 0px;
        padding-bottom: 0px;
    }
    .deliverymethds > li {
        padding-top: 0px;
        padding-bottom: 0px;
    }

    .plr0 {
        padding-left: 0px;
        padding-right: 0px;
    }

    .editlink {
        color: #777;
        border: solid 1px #999;
        padding: 2px 10px;
        text-decoration: none;
        float: right;
        font-size: 10px;
    }

    .cart-img-cust img {
        width: 60px;
        height: 60px;
    }

    .cart-img-cust {
        width: 20%;
    }

    .cart-content-cust {
        width: 79%;
        padding: 0px;
        padding-bottom: 2px;
    }

    .cart-content-cust h6 {
        margin-bottom: 3px;
    }

    .single-cart-box-cust{
        padding-bottom: 5px;
        border-bottom: 1px dashed #dddd;
    }
    .padding15{
        padding: 15px;
    }
    .cart_totals table th {
        font-size: 12px;
    }
    .cart_totals table tr.order-total th{
        font-size: 14px;
    }
    .cart_totals table tr.order-total .amount {
        font-size: 16px;
    }
    @media (max-width: 767px) {
        .container {
            width: auto !important;
        }
    }
</style>

<!-- coupon-area end -->
<div class="container">

    <div class="col-sm-8">

        <?php
        if ($this->loggedIn) {?>
            <div class="panel panel-default no-border-radius">
                <div class="panel-body">
                    <h5 class="ptb10"><span id="showcartDetail">Delivery Address</span></h5>
                    <?php
                    echo shop_form_open('order', 'class="validate"');
                    ?>

                    <?php
                    $r = 1;
                    if (!empty($addresses)) {
                        $country_checked=0;
                        foreach ($addresses as $address) {
                            ?>
                            <div class="col-sm-4 padding2">
                                <div class="panel panel-default no-border-radius">
                                    <div class="panel-body padding0">
                                        <div class="checkbox bg">
                                            <label class="padding0 width">

                                                <strong style="width: 85%;"><?= $address->city; ?></strong>
                                                <?php if($this->Settings->country_name == $address->country ){
                                                    if($address_id ==0){
                                                        $country_checked = $country_checked +1;?>
                                                        <input country_code="<?= $this->site->countryCode($address->country); ?>" state_name="<?= $address->state; ?>" address_id="<?= $address->id; ?>" other_country="0"  country_name="<?= $address->country; ?>" country_code="<?= $address->country_code; ?>" currency_code="<?= $address->currency_code; ?>" city_name="<?= $address->city; ?>" postal_code = "<?= $address->postal_code ?>" style="width: 14%; float: right;" type="radio" name="address"
                                                               value="<?= $address->id; ?>" <?= $country_checked == 1 ? 'checked' : ''; ?>>
                                                    <?php }else{ $country_checked = $country_checked +1;?>

                                                        <input country_code="<?= $this->site->countryCode($address->country); ?>" state_name="<?= $address->state; ?>" address_id="<?= $address->id; ?>" other_country="0"  country_name="<?= $address->country; ?>" country_code="<?= $address->country_code; ?>" currency_code="<?= $address->currency_code;?>" city_name="<?= $address->city; ?>" postal_code = "<?= $address->postal_code ?>"  style="width: 14%; float: right;" type="radio" name="address"
                                                               value="<?= $address->id; ?>" <?= $address->id == $address_id ? 'checked' : ''; ?>>

                                                    <?php } ?>
                                                <?php }else{ ?>
                                                    <input country_code="<?= $this->site->countryCode($address->country); ?>" state_name="<?= $address->state; ?>" address_id="<?= $address->id;?>" other_country="1"  country_name="<?= $address->country; ?>" country_code="<?= $address->country_code; ?>" currency_code="<?= $address->currency_code; ?>" city_name="<?= $address->city; ?>" postal_code = "<?= $address->postal_code ?>" style="width: 14%; float: right;" type="radio" name="address"
                                                    value="<?= $address->id; ?>">

                                                <?php } ?>

                                                <span>
                                    <p class="pt5">
                                       <?= $address->line1; ?><br> <?= $address->line2; ?>
                                    </p>
                                     <p class="pt5"><?= $address->phone; ?></p>
                                     <p class="pt5"><?= $address->state; ?></p>
                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="panel-footer ">
                                        <span class="label label-success no-border-radius"><?= $address->country; ?></span>


                                        <a href="#" class="edit-address pull-right" data-id="<?= $address->id; ?>">
                                            <span class="edit"><i class="fa fa-edit"></i></span>
                                        </a>


                                    </div>
                                </div>
                            </div>
                            <?php
                            if ($r % 3 == 0) {
                                echo '<div class="clearfix"></div>';
                            }
                            $r++;
                        }
                    }
                    ?>
                    <div class="row margin-bottom-lg">
                        <div class="col-sm-12"><a href="#" id="add-address" class="newsletter-btn pwd-btn-width">ADD NEW
                                ADDRESS</a></div>
                    </div>

                    <div id="payment_method_place"></div>


                    <h5 class="hide ptb10"><span id="showcartDetail">Select Delivery Method</span></h5>
                    <ul class="hide list-group method deliverymethds">
                        <li class="list-group-item">
                            <div class="checkbox bg">
                                <label class="width">
                                    <input type="radio" name="payment" class="payment_method" value="3">
                                    <span>
                                    <b>Standard Delivery</b><br>
                                    <small style="margin-left: 10px;">Delivery Fees: <b>9 AED</b></small>
                                </span>
                                </label>
                            </div>

                        </li>
                        <li class="list-group-item">
                            <div class="checkbox bg">
                                <label class="width">
                                    <input type="radio" name="payment" class="payment_method" value="3">
                                    <span>
                                    <b>Pick Up</b><br>
                                    <small style="margin-left: 10px;">Processing Fees: <b>6 AED</b></small>
                                </span>
                                </label>
                            </div>
                        </li>
                    </ul>

                    <!-- <div class="order-button-payment col-sm-4 plr0">
                         <input type="submit" name="add_order" value="Place Order" class="">
                     </div>-->
                    <input type="hidden" id="pt_merchant_id" name="pt_merchant_id" value="">
                    <textarea name="pt_secret_key" style="display: none;" id="pt_secret_key"></textarea>
                    <div class="order-button-payment col-sm-12 plr0">
                        <?php

                        if (!empty($addresses) && !$this->Staff) {
                            echo form_submit('add_order', 'Place Order', 'class="check_out_btn"');
                        } elseif ($this->Staff) {
                            echo '<div class="alert alert-warning margin-bottom-no">' . lang('staff_not_allowed') . '</div>';
                        } else {
                            echo '<div class="alert alert-warning margin-bottom-no">' . lang('please_add_address_first') . '</div>';
                        }
                        echo form_close();
                        ?>
                    </div>

                </div>
            </div>
        <?php } else{?>


            <div class="panel-body" id="checkout-login"  style=" <?= $this->loggedIn ? 'display:none;' : 'display:block; padding: 0px;' ?>">

                <div>
                    <?php
                    if (!$this->loggedIn) {
                        ?>
                        <ul class="nav nav-tabs hide" role="tablist">
                            <li role="presentation" class="active"><a href="#user" aria-controls="user" role="tab" data-toggle="tab"><?= lang('returning_user'); ?></a></li>
                            <li role="presentation"><a href="#guest" aria-controls="guest" role="tab" data-toggle="tab"><?= lang('guest_checkout'); ?></a></li>
                        </ul>
                        <?php
                    }
                    ?>

                    <div  class="coupon-content"
                          style=" <?= $this->loggedIn ? 'display:none;' : 'display:block;' ?>">

                        <div class="coupon-info">
                            <p class="coupon-text"></p>

                            <div class="row">
                                <div class="col-sm-6">
                                    <h2>New Customer</h2>
                                    <p>By creating an account you will be able to shop faster, be up to date on an
                                        order's status, and keep track of the orders you have previously made.</p>
                                    <a href="<?= base_url() . 'register'; ?>" class="newsletter-btn">Continue</a>
                                </div>
                                <div class="col-sm-6 ">
                                    <form action="<?= base_url() ?>login" class="validate fv-form fv-form-bootstrap"
                                          accept-charset="utf-8" novalidate="novalidate" method="post"
                                          enctype="multipart/form-data">

                                        <p class="form-row-first form-group">
                                            <label>Username or email <span class="required">*</span></label>
                                            <input type="text" name="identity" value=""
                                                   placeholder="Username or email" id="username"
                                                   class="form-control" required>
                                        </p>
                                        <p class="form-row-last form-group">
                                            <label>Password <span class="required">*</span></label>
                                            <input type="password" name="password" value="" placeholder="Password"
                                                   id="password" class="form-control" required>
                                        </p>
                                        <p class="form-row">
                                            <input type="submit" value="Login"/>
                                            <label>
                                                <input type="checkbox"/>
                                                Remember me
                                            </label>
                                        </p>
                                        <p class="lost-password">
                                            <a class="forgot-password" href="#">Lost your password?</a>
                                        </p>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="tab-content padding-lg hide">
                        <div role="tabpanel" class="tab-pane fade in active" id="user">
                            <div  class="coupon-content"
                                  style=" <?= $this->loggedIn ? 'display:none;' : 'display:block;' ?>">

                                <div class="coupon-info">
                                    <p class="coupon-text"></p>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h2>New Customer</h2>
                                            <p>By creating an account you will be able to shop faster, be up to date on an
                                                order's status, and keep track of the orders you have previously made.</p>
                                            <a href="<?= base_url() . 'register'; ?>" class="newsletter-btn">Continue</a>
                                        </div>
                                        <div class="col-sm-6 ">
                                            <form action="<?= base_url() ?>login" class="validate fv-form fv-form-bootstrap"
                                                  accept-charset="utf-8" novalidate="novalidate" method="post"
                                                  enctype="multipart/form-data">

                                                <p class="form-row-first form-group">
                                                    <label>Username or email <span class="required">*</span></label>
                                                    <input type="text" name="identity" value=""
                                                           placeholder="Username or email" id="username"
                                                           class="form-control" required>
                                                </p>
                                                <p class="form-row-last form-group">
                                                    <label>Password <span class="required">*</span></label>
                                                    <input type="password" name="password" value="" placeholder="Password"
                                                           id="password" class="form-control" required>
                                                </p>
                                                <p class="form-row">
                                                    <input type="submit" value="Login"/>
                                                    <label>
                                                        <input type="checkbox"/>
                                                        Remember me
                                                    </label>
                                                </p>
                                                <p class="lost-password">
                                                    <a class="forgot-password" href="#">Lost your password?</a>
                                                </p>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="guest" style="margin: 10px;">
                            <?= shop_form_open('order', 'class="validate" id="guest-checkout"'); ?>
                            <input type="hidden" value="1" name="guest_checkout">
                            <div class="row " style="border: 1px solid #e5e5e5; padding: 20px;">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?= lang('name', 'name'); ?> *
                                                <?= form_input('name', set_value('name'), 'class="form-control" id="name" required="required"'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?= lang('company', 'company'); ?>
                                                <?= form_input('company', set_value('company'), 'class="form-control" id="company"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang('email', 'email'); ?> *
                                        <?= form_input('email', set_value('email'), 'class="form-control" id="email" required="required"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang('phone', 'phone'); ?> *
                                        <?= form_input('phone', set_value('phone'), 'class="form-control" id="phone" required="required"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h5><strong><?= lang('billing_address'); ?></strong></h5>
                                    <input type="hidden" value="new" name="address">
                                    <hr>
                                    <div class="form-group">
                                        <?= lang('line1', 'billing_line1'); ?> *
                                        <?= form_input('billing_line1', set_value('billing_line1'), 'class="form-control" id="billing_line1" required="required"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang('line2', 'billing_line2'); ?>
                                        <?= form_input('billing_line2', set_value('billing_line2'), 'class="form-control" id="billing_line2" required="required"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?= lang('city', 'billing_city'); ?> *
                                                <?= form_input('billing_city', set_value('billing_city'), 'class="form-control" id="billing_city" required="required"'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?= lang('postal_code', 'billing_postal_code'); ?>
                                                <?= form_input('billing_postal_code', set_value('billing_postal_code'), 'class="form-control" id="billing_postal_code"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang('state', 'billing_state'); ?>
                                        <?php
                                        if ($Settings->indian_gst) {
                                            $states = $this->gst->getIndianStates();
                                            echo form_dropdown('billing_state', $states, '', 'class="form-control selectpicker mobile-device" id="billing_state" title="Select" required="required"');
                                        } else {
                                            echo form_input('billing_state', '', 'class="form-control" id="billing_state"');
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang('country', 'billing_country'); ?> *
                                        <?= form_input('billing_country', set_value('billing_country'), 'class="form-control" id="billing_country" required="required"'); ?>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="checkbox bg pull-right" style="margin-top: 0; margin-bottom: 0;">
                                        <label>
                                            <input type="checkbox" name="same" value="1" id="same_as_billing">
                                            <span>
                                                                <?= lang('same_as_billing') ?>
                                                            </span>
                                        </label>
                                    </div>
                                    <h5><strong><?= lang('shipping_address'); ?></strong></h5>
                                    <input type="hidden" value="new" name="address">
                                    <hr>
                                    <div class="form-group">
                                        <?= lang('line1', 'shipping_line1'); ?> *
                                        <?= form_input('shipping_line1', set_value('shipping_line1'), 'class="form-control" id="shipping_line1" required="required"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang('line2', 'shipping_line2'); ?>
                                        <?= form_input('shipping_line2', set_value('shipping_line2'), 'class="form-control" id="shipping_line2" required="required"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?= lang('city', 'shipping_city'); ?> *
                                                <?= form_input('shipping_city', set_value('shipping_city'), 'class="form-control" id="shipping_city" required="required"'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?= lang('postal_code', 'shipping_postal_code'); ?>
                                                <?= form_input('shipping_postal_code', set_value('shipping_postal_code'), 'class="form-control" id="shipping_postal_code"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang('state', 'shipping_state'); ?>
                                        <?php
                                        if ($Settings->indian_gst) {
                                            $states = $this->gst->getIndianStates();
                                            echo form_dropdown('shipping_state', $states, '', 'class="form-control selectpicker mobile-device" id="shipping_state" title="Select" required="required"');
                                        } else {
                                            echo form_input('shipping_state', '', 'class="form-control" id="shipping_state"');
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang('country', 'shipping_country'); ?> *
                                        <?= form_input('shipping_country', set_value('shipping_country'), 'class="form-control" id="shipping_country" required="required"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang('phone', 'shipping_phone'); ?> *
                                        <?= form_input('shipping_phone', set_value('shipping_phone'), 'class="form-control" id="shipping_phone" required="required"'); ?>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <h5><strong><?= lang('payment_method'); ?></strong></h5>
                                    <hr>
                                    <div class="checkbox bg">
                                        <?php if ($paypal->active) { ?>
                                            <label style="display: inline-block; width: auto;">
                                                <input type="radio" name="payment_method" value="paypal" id="paypal" required="required">
                                                <span>
                                                                <i class="fa fa-paypal margin-right-md"></i> <?= lang('paypal') ?>
                                                            </span>
                                            </label>
                                        <?php } ?>
                                        <?php if ($skrill->active) { ?>
                                            <label style="display: inline-block; width: auto;">
                                                <input type="radio" name="payment_method" value="skrill" id="skrill" required="required">
                                                <span>
                                                                <i class="fa fa-credit-card-alt margin-right-md"></i> <?= lang('skrill') ?>
                                                            </span>
                                            </label>
                                        <?php } ?>


                                        <label style="display: inline-block; width: auto;">
                                            <input type="radio" name="payment_method" value="cod" id="cod" required="required">
                                            <span>
                                                                <i class="fa fa-money-bill-alt margin-right-md"></i> Cash On Delivery
                                                            </span>
                                        </label>
                                    </div>
                                </div>





                                <?= form_submit('guest_order', lang('submit'), 'class="newsletter-btn"'); ?>

                            </div>

                            <?= form_close(); ?>
                        </div>
                    </div>

                </div>

            </div>

        <?php } ?>
    </div>
    <div class="col-sm-4 padding0">
        <div class="panel panel-default no-border-radius">
            <div class="panel-body padding0">
                <div class="padding15">
                    <h5 class="ptb10" style="display: inline;"><span id="showcartDetail">Shopping Cart <small>(<span class="cart-total-items"></span> items)</small></span>
                    </h5>
                    <a href="<?= base_url() . 'cart'; ?>" class="editlink">Edit Cart</a>
                    <hr>
                    <div id="cart-contents">
                        <div id="cart-items1">
                        </div>
                    </div>
                </div>

                <div class="cart_totals cart_totals-cust padding15" style="background: #EDECEA">
                    <table id="cart-totals">
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="giftcart_amount" value="<?php if($this->session->userdata('gift_cart_amount')){ echo $this->session->userdata('gift_cart_amount');} ?>">
<script type="text/javascript">
        <?php if($change_new_country == 1 && isset($address_all_data->country_code)){ ?>
        $(document).ready(function () {
            check_new_added_country(<?= json_encode($address_all_data->country_code) ?>,<?= json_encode($address_all_data->currency_code); ?>,<?= json_encode($address_all_data->id) ?>);
        });
        <?php } ?>
    var addresses = <?= !empty($addresses) ? json_encode($addresses) : 'false'; ?>;
</script>

